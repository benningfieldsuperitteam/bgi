'use strict';

module.exports = function(grunt) {
  const VERBOSE    = false;

  const buildDir   = __dirname + '/build/';
  const tmpDir     = __dirname + '/.tmp/';
  const angAppName = 'bgi';

  // This object contains all scripts for the application.  The object has
  // properties describing each type of script (app, grunt, unitTests, etc.).
  // Pass true for verbose output.
  const scripts = require('./grunt/scriptGarner')(VERBOSE, angAppName);

  // This array contains all the html files for the application.  These files
  // are partial files, and are added to a javascript file as part of the
  // ngtemplates task.
  const html = require('./grunt/htmlGarner')(VERBOSE);

  grunt.initConfig({
    clean:          require('./grunt/clean')(grunt, buildDir, tmpDir),
    jshint:         require('./grunt/jshint')(grunt, scripts),
    karma:          require('./grunt/karma')(grunt, scripts, html),
    copy:           require('./grunt/copy')(grunt),
    ngtemplates:    require('./grunt/ngtemplates')(grunt, html, tmpDir, angAppName),
    concat:         require('./grunt/concat')(grunt, scripts, tmpDir, angAppName),
    babel:          require('./grunt/babel')(grunt, tmpDir, angAppName),
    uglify:         require('./grunt/uglify')(grunt, tmpDir, buildDir, angAppName),
    sass:           require('./grunt/sass')(grunt, angAppName)
  });

  // Clean the temp and build directories.
  grunt.registerTask('clean_prebuild',  ['clean:tmp', 'clean:build']);

  // Copy static assets to the build directory.
  grunt.registerTask('copy_assets', ['copy:fonts']);

  // Build the native (desktop) application.
  grunt.registerTask('build', [
    'clean_prebuild',          // Remove the old build and .tmp directories.
    'jshint',                  // Check for lint.
    'karma:single',            // Make sure the client-side tests are passing.
    'copy_assets',             // Copy any static assets, such as fonts.
    'ngtemplates',             // Put all template HTML into a script (preload angular cache).
    'sass',                    // Compile SCSS to CSS
    'concat',                  // Concatenate all JS.
    'babel',                   // Compile ES6.
    'uglify',                  // Minify JS.
    'clean:tmp'                // Remove the temporary folder.
  ]);

  // By default just build the native (desktop) application.
  grunt.registerTask('default', ['build']);
};
