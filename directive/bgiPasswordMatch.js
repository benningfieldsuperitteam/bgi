(function(angular) {
  'use strict';

  angular.module('bgi')
    .directive('bgiPasswordMatch', ['$parse', passwordMatchProducer]);

  function passwordMatchProducer($parse) {
    return {
      restrict : 'A',
      require  : 'ngModel',
      link     : function link(scope, ele, attrs, ctrl) {
        // Function to get the password.
        const getPW  = $parse(attrs.bgiPasswordMatch).bind(null, scope);

        // Function to get the password verification.
        const getVPW = () => ctrl.$modelValue;

        // If either the password or the password verification changes, check
        // if the two match.
        scope.$watch(getPW,  pw  => checkPasswordMatch(pw,      getVPW()));
        scope.$watch(getVPW, vpw => checkPasswordMatch(getPW(), vpw));

        // Set the validity of the form field based on the password matching.
        function checkPasswordMatch(pw, vpw) {
          ctrl.$setValidity('password-match', pw === vpw);
        }
      }
    };
  }
})(window.angular);

