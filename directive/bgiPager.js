(function(angular) {
  'use strict';

  angular.module('bgi')

  /**
   * A pager directive.
   */
  .directive('bgiPager', bgiPager);

  function bgiPager() {
    const ddo = {
      template: template,
      restrict: 'E',
      scope   : {
        pager: '=pageUsing'
      },
      link    : link,
    };

    function link(scope) {
      // Show the pager.
      scope.show = function() {
        return scope.pager.getNumPages() > 1;
      };

      // Disable previous/first functionality.
      scope.isDisablePrev = function() {
        return scope.pager.getPageNum() === 1;
      };

      // Disable next/last functionality.
      scope.isDisableNext = function() {
        return scope.pager.getPageNum() === scope.pager.getNumPages();
      };

      // Check if the page is active.
      scope.isActivePage = function(pageNum) {
        return scope.pager.getPageNum() === pageNum;
      };

      scope.itemsPerPage = function(itemsPerPage) {
        if (itemsPerPage !== undefined) {
          scope.pager.setItemsPerPage(itemsPerPage);
        }

        return scope.pager.getItemsPerPage();
      };
    }

    function template() {
      return `
        <div class="row bgi-pager">
          <div class="col-xs-12 col-sm-6">
            <ul class="pagination" ng-show="show()">
              <li ng-class="{disabled: isDisablePrev()}">
                <a ng-click="pager.first()">&laquo;</a>
              </li>

              <li ng-class="{disabled: isDisablePrev()}">
                <a ng-click="pager.prev()">&lsaquo;</a>
              </li>

              <li ng-repeat="pageNum in pager.getPageRange()" ng-class="{active: isActivePage(pageNum)}">
                <a ng-bind="pageNum" ng-click="pager.setPageNum(pageNum)"></a>
              </li>

              <li ng-class="{disabled: isDisableNext()}">
                <a ng-click="pager.next()">&rsaquo;</a>
              </li>

              <li ng-class="{disabled: isDisableNext()}">
                <a ng-click="pager.last()">&raquo;</a>
              </li>
            </ul>
          </div>

          <div class="col-xs-12 col-sm-6 items-per-page">
            <div class="form-inline">
              <label>Items Per Page</label>
              <select class="form-control" ng-options="i for i in [10,25,50]"
                ng-model="itemsPerPage" ng-model-options="{getterSetter: true}" ></select>
            </div>
          </div>
        </div>
      `;
    }

    return ddo;
  }
})(window.angular);
