(function() {
  'use strict';

  angular.module('bgi')
    .component('bgiErrorMessage', {
      template     : `
        <div class="error" ng-if="vm.hasError()">
          <!-- Top-level error or single error. -->
          <div ng-bind="vm.getError().detail" class="alert alert-danger"></div>

          <!-- Sub-errors. -->
          <div ng-repeat="err in vm.getError().errors" ng-bind="err.detail" class="alert alert-danger"></div>
        </div>`,
      controller   : ['bgi_errorMessageSvc', BGIErrorMessageCtrl],
      controllerAs : 'vm',
      bindings     : {
        error : '<'
      }
    });

  function BGIErrorMessageCtrl(errMsgSvc) {
    const vm         = this;
    let   localError = null;

    vm.hasError   = hasError;
    vm.getError   = getError;
    vm.$onInit    = checkNewError;
    vm.$onChanges = checkNewError;

    // Check if an error is set.
    function hasError() {
      return localError !== null;
    }

    // Get the error.
    function getError() {
      return localError;
    }

    // When the error changes.
    function checkNewError() {
      // Clear the local error (the copy) and standardize the actual error.
      localError = null;

      errMsgSvc
        .standardizeError(vm.error)
        .catch(err => localError = err);
    }
  }
})();

