(function(angular) {
  'use strict';

  angular.module('bgi')
    .component('bgiLoader', {
      controller: ['bgi_loader', BGILoaderCtrl],
      controllerAs: 'vm',
      template: `
        <div class="loader" ng-if="vm.isLoading()">
          <div class="spinner"></div>
          <div class="text-center mar-t-md load-text">Loading...</div>
        </div>`
    });

  function BGILoaderCtrl(loader) {
    const vm = this;

    vm.isLoading = () => loader.isLoading();
  }
})(window.angular);
