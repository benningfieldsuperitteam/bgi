(function(angular) {
  'use strict';

  angular
    .module('bgi')
    .directive('bgiCopyToClipboard', ['$parse', '$document', bgiCopyToClipboardProducer]);

  function bgiCopyToClipboardProducer($parse, $document) {
    return {
      restrict : 'A',
      link     : function link(scope, ele, attrs) {
        const getText  = $parse(attrs.bgiCopyToClipboard);
        const document = $document[0];

        ele.on('click', copyText);
        scope.$on('$destroy', () => ele.off('click', copyText));

        function copyText() {
          const text = getText(scope);

          // If there's text supplied, then it needs to be copied.
          if (text && typeof text === 'string') {
            // Create a temporary text element, add the text to it, and select the text.
            const textEle = document.createElement('textarea');
            textEle.value = text;
            document.body.appendChild(textEle);
            textEle.select();

            // Copy to the clipboard and then remove the temporary element.
            document.execCommand('copy');
            document.body.removeChild(textEle);
          }
        }
      }
    };
  }

})(window.angular);

