describe('bgiErrorMessageSpec test suite -', function() {
  'use strict';

  let $componentController, $q, $rootScope, bindings;

  beforeEach(module('bgi'));
  beforeEach(inject(function(_$componentController_, _$q_, _$rootScope_) {
    $componentController = _$componentController_;
    $q                   = _$q_;
    $rootScope           = _$rootScope_;

    // For testing.  Used below.
    bindings = {error: null};
  }));

  it('checks that the controller exposes hasError and getError methods.', function() {
    const ctrl = $componentController('bgiErrorMessage', null, bindings);
    expect(ctrl.hasError).toBeDefined();
    expect(ctrl.getError).toBeDefined();
    expect(ctrl.hasError()).toBe(false);
    expect(ctrl.getError()).toBe(null);
  });

  it('checks that error.detail takes precedence over error.message.', function() {
    bindings.error = {message: 'MSG', detail: 'DET'};
    const ctrl = $componentController('bgiErrorMessage', null, bindings);
    ctrl.$onInit();
    $rootScope.$apply();
    expect(ctrl.hasError()).toBe(true);
    expect(ctrl.getError().detail).toBe('DET');
  });

  it('checks that a promise can be used in place of an error.', function() {
    const defer = $q.defer();
    bindings.error = defer.promise;
    const ctrl = $componentController('bgiErrorMessage', null, bindings);
    ctrl.$onInit();
    expect(ctrl.hasError()).toBe(false);

    defer.reject(new Error('Mock'));
    $rootScope.$apply();
    expect(ctrl.hasError()).toBe(true);
    expect(ctrl.getError().detail).toBe('Mock');
  });
});

