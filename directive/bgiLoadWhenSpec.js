describe('directive: ceaExamLoadWhen', function() {
  'use strict';

  let $compile,
      $rootScope;

  beforeEach(module('bgi'));
  beforeEach(inject(function(_$compile_, _$rootScope_) {
    $compile   = _$compile_;
    $rootScope = _$rootScope_;
  }));

  describe('init -', function() {
    it('wraps the content of the button with a loading div.', function() {
      const scope = $rootScope.$new(),
            ele   = $compile('<button bgi-load-when="prom">Click Me</button>')(scope);

      expect(ele.children().length).toBe(1);

      const inner = ele.children().eq(0);
      expect(inner.hasClass('loading-button-inner')).toBe(true);
      expect(inner.children().length).toBe(1);
      // No spinner until a scope apply.  Spinner tests below.

      const content = inner.children().eq(0);
      expect(content.hasClass('loading-button-content')).toBe(true);
      expect(content.text()).toBe('Click Me');
    });

    it('has an initially ACTIVE state.', function() {
      const scope    = $rootScope.$new(),
            ele      = $compile('<button bgi-load-when="prom">Click Me</button>')(scope),
            dirScope = ele.scope();

      expect(dirScope.button.state).toBe('ACTIVE');
    });

    it('changes the state to LOADING when a promise is added.', inject(function($q) {
      const scope    = $rootScope.$new(),
            ele      = $compile('<button bgi-load-when="prom">Click Me</button>')(scope),
            inner    = ele.children().eq(0),
            dirScope = ele.scope(),
            defer    = $q.defer();

      scope.$apply();
      expect(dirScope.button.state).toBe('ACTIVE');
      expect(ele.attr('disabled')).not.toBeDefined();
      expect(inner.hasClass('loading')).toBe(false);

      scope.prom = defer.promise;
      scope.$apply();
      expect(dirScope.button.state).toBe('LOADING');
      expect(ele.attr('disabled')).toBe('disabled');
      expect(inner.hasClass('loading')).toBe(true);

      defer.resolve();
      scope.$apply();
      expect(dirScope.button.state).toBe('ACTIVE');
      expect(ele.attr('disabled')).not.toBeDefined();
      expect(inner.hasClass('loading')).toBe(false);
    }));

    it('shows the loading spinner when loading by default.', function() {
      const scope    = $rootScope.$new(),
            ele      = $compile('<button bgi-load-when="prom">Click Me</button>')(scope),
            inner    = ele.children().eq(0),
            dirScope = ele.scope();
      let   spinner;

      // No spinner until a digest happens.
      expect(inner.children().length).toBe(1);
      scope.$apply();
      expect(inner.children().length).toBe(2);
      spinner = inner.children().eq(1);
      expect(spinner.hasClass('spinner')).toBe(true);

      expect(dirScope.button.showSpinner()).toBe(true);
    });

    it('allows the spinner to be enabled explicitly using a "show-spinner" attribute.', function() {
      const scope    = $rootScope.$new(),
            ele      = $compile('<button bgi-load-when="prom" show-spinner="true">Click Me</button>')(scope),
            dirScope = ele.scope();

      expect(dirScope.button.showSpinner()).toBe(true);
    });

    it('allows the spinner to be disabled using the "show-spinner" attrubite.', function() {
      const scope    = $rootScope.$new(),
            ele      = $compile('<button bgi-load-when="prom" show-spinner="false">Click Me</button>')(scope),
            dirScope = ele.scope();

      expect(dirScope.button.showSpinner()).toBe(false);
    });

    it('allows an expression be passed in the "show-spinner" attribute.', function() {
      const scope    = $rootScope.$new(),
            ele      = $compile('<button bgi-load-when="prom" show-spinner="showSpinner()">Click Me</button>')(scope),
            dirScope = ele.scope();
      let rVal       = true;

      scope.showSpinner = () => rVal;

      expect(dirScope.button.showSpinner()).toBe(true);
      rVal = false;
      expect(dirScope.button.showSpinner()).toBe(false);
    });
  });
});

