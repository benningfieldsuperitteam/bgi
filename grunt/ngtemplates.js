module.exports = function(grunt, html, tmpDir, angAppName) {
  'use strict';

  const ngtemplates = {
    [angAppName]: {
      src:    html,
      cwd:    __dirname + '/../',
      dest:   `${tmpDir}js/templates.js`,
      options: {
        htmlmin: {
          removeComments:     true,
          collapseWhitespace: true
        }
      }
    }
  };

  grunt.loadNpmTasks('grunt-angular-templates');

  return ngtemplates;
};

