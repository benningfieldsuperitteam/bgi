module.exports = function(grunt, angAppName) {
  'use strict';

  const sass = {
    dist: {
      files: {
        [`build/${angAppName}.css`] : `${__dirname}/../scss/${angAppName}.scss`
      },
      options: {
        sourceMap: false
      }
    }
  };

  grunt.loadNpmTasks('grunt-sass');

  return sass;
};
