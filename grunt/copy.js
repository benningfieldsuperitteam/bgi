module.exports = function(grunt) {
  'use strict';

  const copy = {
    // All fonts are copied to the build.
    fonts: {
      cwd:   __dirname + '/../',
      src:   'fonts/**/*',
      dest:  'build',
      expand: true
    }
  };

  grunt.loadNpmTasks('grunt-contrib-copy');

  return copy;
};

