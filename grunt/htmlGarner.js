'use strict';

module.exports = function(verbose) {
  const glob = require('glob');
  const opts = {
    ignore: [
      'bower_components/**',
      'node_modules/**'
    ]
  };

  // All the HTML files in the application that should be built.
  const html = glob.sync('**/*.html', opts);

  if (verbose) {
    console.log('htmlGarner gathered the following files.\n');
    console.log(html);
    console.log('\n');
  }

  return html;
};

