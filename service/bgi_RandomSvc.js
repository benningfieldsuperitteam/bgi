(function(angular) {
  'use strict';

  angular.module('bgi')
    .factory('bgi_RandomSvc', [bgiRandomSvcProducer]);

  function bgiRandomSvcProducer() {
    class bgiRandomNumberSvc {
      /**
       * Create a random string of length (defaults to 10).
       */
      static generateRandomString(length=10) {
        let randStr = '';

        for (let i = 0; i < length; ++i) {
          randStr += Math.floor(Math.random() * 36).toString(36);
        }

        return randStr;
      }
    }

    return bgiRandomNumberSvc;
  }
})(window.angular);

