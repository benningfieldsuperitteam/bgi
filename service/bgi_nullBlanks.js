(function(angular) {
  'use strict';

  angular.module('bgi')
    .factory('bgi_nullBlanks', [bgi_nullBlanksProducer]);

  function bgi_nullBlanksProducer() {
    // Helper function to convert any empty strings to null in an object.
    return function nullBlanks(obj) {
      for (let key in obj) {
        if (typeof obj[key] === 'string' && obj[key].trim() === '') {
          obj[key] = null;
        }
      }

      return obj;
    };
  }
})(window.angular);

