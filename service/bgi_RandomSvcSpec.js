describe('bgiRandomSvc().', function() {
  'use strict';

  let RandomSvc;

  // Initialize the modules.
  beforeEach(function() {
    angular.mock.module('bgi');
  });

  beforeEach(inject(function(bgi_RandomSvc) {
    RandomSvc = bgi_RandomSvc;
  }));

  describe('generateRandomString()', function() {
    it('defaults to 10 characters.', function() {
      expect(RandomSvc.generateRandomString().length).toBe(10);
    });

    it('accepts a length argument, and returns a string of that length.', function() {
      expect(RandomSvc.generateRandomString(20).length).toBe(20);
    });
  });
});

