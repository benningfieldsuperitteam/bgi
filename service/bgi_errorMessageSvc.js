(function() {
  'use strict';

  angular.module('bgi')
    .factory('bgi_errorMessageSvc', ['$q', errorMessageSvcProducer]);

  function errorMessageSvcProducer($q) {
    /**
     * Helper for application error messages.
     */
    class ErrorMessageSvc {
      /**
       * Standardize an error message.
       * @memberOf ErrorMessageSvc
       * @param err {object} An error object containing at least a message or a
       *        detail property.  The detail property takes precedence over
       *        message if both are set.  Alternatively, err may be a promise
       *        that may be rejected with an error.
       * @return {promise} A promise object that is rejected with an error
       *         object, or resolved if there is no error.  The format shall be:
       *         {
       *           code   : {string},
       *           detail : {string},
       *           errors : {array<error>}
       *         }
       */
      standardizeError(err) {
        // Convert err to an error object in the following format:
        function convert(err) {
          // API-side errors are in the 'data' property.
          if (err.data) {
            return convert(err.data);
          }
          else {
            const stdErr = {
              code   : err.code   || null,
              detail : err.detail || err.message,
              errors : []
            };

            if (err.errors) {
              stdErr.errors = err.errors.map(convert);
            }

            return stdErr;
          }
        }

        // No error passed in.
        if (!err) {
          return $q.resolve();
        }

        // Error has a 'catch' property.  Treat it as a promise.
        if (err.catch) {
          // The promise is converted to an Angular-based promise for
          // consistency, hence the $q.when wrapper.
          return $q
            .when(err)
            .catch(err => $q.reject(convert(err)));
        }

        // Error is an error instance.
        return $q.reject(convert(err));
      }
    }

    return new ErrorMessageSvc();
  }
})();

