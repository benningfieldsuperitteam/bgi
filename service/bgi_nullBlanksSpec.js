describe('nullBlanks()', function() {
  'use strict';

  let nullBlanks;

  beforeEach(module('bgi'));
  beforeEach(inject(function(_bgi_nullBlanks_) {
    nullBlanks = _bgi_nullBlanks_;
  }));

  it('converts blank strings to null.', function() {
    const obj = {
      name: '',
      age: 30
    };

    expect(nullBlanks(obj).name).toBeNull();
    expect(nullBlanks(obj).age).toBe(30);
  });

  it('trims null strings first.', function() {
    const obj = {
      name: '    ',
      age: 30
    };

    expect(nullBlanks(obj).name).toBeNull();
    expect(nullBlanks(obj).age).toBe(30);
  });
});
