(function() {
  'use strict';

  angular.module('bgi')
    .factory('bgi_storageSvc', ['$window', bgiStorageSvcProducer]);

  function bgiStorageSvcProducer($window) {
    /**
     * Class for moving objects in and out of local storage.
     */
    class StorageSvc {
      /**
       * Initialize the object.
       */
      constructor() {
        // This is for caching.
        this._store = {};
      }

      /**
       * Store an object.
       * @param key The key associated with the object.
       * @param obj The object to store.
       * @return The object that was stored.
       */
      setObject(key, obj) {
        this._store[key] = obj;
        $window.localStorage.setItem(key, $window.JSON.stringify(obj));
        return obj;
      }

      /**
       * Get the object associated with key.
       * @param key The key associated with the object.
       */
      getObject(key) {
        // The object isn't in cache - pull it from local storage.
        if (this._store[key] === undefined) {
          const strVal = $window.localStorage.getItem(key);

          // Item doesn't exist in local storage.
          if (strVal === null) {
            return null;
          }

          this._store[key] = $window.JSON.parse(strVal);
        }

        return this._store[key];
      }

      /**
       * Remove the item associated with key.
       * @param key The key associated with the item to delete.
       */
      removeObject(key) {
        delete this._store[key];
        $window.localStorage.removeItem(key);
      }
    }

    // Single instance.
    return new StorageSvc();
  }
})();

