describe('bgi_storageSvc test suite.', function() {
  'use strict';

  // Initialize the modules.
  beforeEach(function() {
    angular.mock.module('bgi');
  });

  beforeEach(inject(function($window) {
    // Track calls to localStorage methods.
    spyOn($window.Storage.prototype, 'setItem').and.callThrough();
    spyOn($window.Storage.prototype, 'getItem').and.callThrough();
    spyOn($window.Storage.prototype, 'removeItem').and.callThrough();

    // Clear local storage ('person' is used for tests) after each test.
    $window.localStorage.removeItem('person');
  }));

  // Checks setObject.
  it('checks setObject', inject(function($window, bgi_storageSvc) {
    const obj = {name: 'Joe Bob', age: 30};
    let parsed;

    expect($window.localStorage.getItem('person')).toBeNull();
    bgi_storageSvc.setObject('person', obj);
    expect($window.localStorage.setItem).toHaveBeenCalled();

    parsed = $window.JSON.parse($window.localStorage.getItem('person'));
    expect(parsed.name).toBe('Joe Bob');
    expect(parsed.age).toBe(30);
  }));

  // Checks getObject.
  it('checks getObject', inject(function($window, bgi_storageSvc) {
    const obj = {name: 'Joe Bob', age: 30};
    let parsed;

    expect($window.localStorage.getItem('person')).toBeNull();
    expect(bgi_storageSvc.getObject('person')).toBeNull(); // 1st call.
    bgi_storageSvc.setObject('person', obj);

    parsed = bgi_storageSvc.getObject('person'); // 2nd call.
    expect($window.localStorage.getItem.calls.count()).toBe(2);

    // Item cached - call again should not hit localStorage.
    parsed = bgi_storageSvc.getObject('person');
    expect($window.localStorage.getItem.calls.count()).toBe(2);
    expect(parsed.name).toBe('Joe Bob');
    expect(parsed.age).toBe(30);
  }));

  // Checks remove.
  it('checks remove', inject(function($window, bgi_storageSvc) {
    const obj = {name: 'Joe Bob', age: 30};

    expect($window.localStorage.getItem('person')).toBeNull();
    bgi_storageSvc.setObject('person', obj);
    bgi_storageSvc.removeObject('person');

    expect($window.localStorage.removeItem).toHaveBeenCalled();
    expect($window.localStorage.getItem('person')).toBeNull();
    expect(bgi_storageSvc.getObject('person')).toBeNull();
  }));
});

