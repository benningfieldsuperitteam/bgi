(function(angular) {
  'use strict';

  angular.module('bgi')
    .provider('bgi_historySvc', [bgiHistorySvcProviderProducer]);

  function bgiHistorySvcProviderProducer() {
    const vm             = this; // jshint ignore:line
    const HIST_STORE_KEY = 'bgi-route-history';

    let blacklist   = [];
    vm.setBlacklist = list => blacklist = list;

    vm.$get = ['$rootScope', 'bgi_storageSvc', bgiHistorySvcProducer];

    function bgiHistorySvcProducer($rootScope, storageSvc) {
      class HistorySvc {
        /**
         * Init.
         */
        constructor() {
          this.maxHistorySize = 5;
          this.history        = storageSvc.getObject(HIST_STORE_KEY) || [];
        }

        /**
         * Initialize the location change listener.
         */
        initialize() {
          $rootScope.$on('$locationChangeSuccess', (e, newURL) => this.addHistory(newURL));
        }

        /**
         * Add a history entry.
         */
        addHistory(URL) {
          // Don't duplicate history.
          // Don't add blacklisted routes.
          if (URL === this.getLast().next().value || blacklist.find(entry => URL.indexOf(entry) === 0)) {
            return;
          }

          this.history.push(URL);

          // Don't exceed the max size.
          if (this.history.length > this.maxHistorySize) {
            this.history = this.history.splice(this.maxHistorySize * -1);
          }

          // Store the array.
          storageSvc.setObject(HIST_STORE_KEY, this.history);
        }

        /**
         * Get the last history item.  Generator function that allows iterating
         * through history.
         */
        *getLast() {
          let index = this.history.length - 1;

          while (index >= 0) {
            yield this.history[index--];
          }
        }

        /**
         * Get the last path.
         */
        *getLastPath() {
          const last = this.getLast();
          let url;

          while ((url = last.next().value)) {
            // Get rid of everything up to the first hash (or hash bang).
            yield url.replace(/^[^#]+#!?/, '');
          }
        }
      }

      return new HistorySvc();
    }
  }
})(window.angular);

