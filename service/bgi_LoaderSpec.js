describe('loader test suite.', function() {
  'use strict';

  let loader, $q, $rootScope;

  beforeEach(module('bgi'));
  beforeEach(inject(function(_bgi_loader_, _$q_, _$rootScope_) {
    loader     = _bgi_loader_;
    $rootScope = _$rootScope_;
    $q         = _$q_;
  }));

  it('checks that the loader loads until a promise is resolved.', function() {
    const defer = $q.defer();
    loader.loadUntil(defer.promise);
    expect(loader.loading).toBe(true);
    defer.resolve();
    $rootScope.$apply();
    expect(loader.loading).toBe(false);
  });

  it('checks that the loader ignores the promise if it\'s already been passed in.', function() {
    const defer = $q.defer();
    loader.loadUntil(defer.promise);
    loader.loadUntil(defer.promise);
    loader.loadUntil(defer.promise);
    expect(loader.loading).toBe(true);
    defer.resolve();
    $rootScope.$apply();
    expect(loader.loading).toBe(false);
  });

  it('checks that the loader will wait on multiple promises.', function() {
    const defer1 = $q.defer();
    const defer2 = $q.defer();
    const defer3 = $q.defer();
    loader.loadUntil(defer1.promise);
    loader.loadUntil(defer2.promise);
    loader.loadUntil(defer3.promise);

    expect(loader.loading).toBe(true);
    defer2.resolve();
    $rootScope.$apply();
    expect(loader.loading).toBe(true);

    defer1.resolve();
    $rootScope.$apply();
    expect(loader.loading).toBe(true);

    defer3.resolve();
    $rootScope.$apply();
    expect(loader.loading).toBe(false);
  });
});
