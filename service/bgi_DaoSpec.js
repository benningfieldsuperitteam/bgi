describe('bgi_Dao()', function() {
  'use strict';

  let Dao, FakeDao, $httpBackend, $rootScope;

  beforeEach(module('bgi'));
  beforeEach(inject(function(_bgi_Dao_, _$httpBackend_, _$rootScope_) {
    $httpBackend = _$httpBackend_;
    $rootScope   = _$rootScope_;
    Dao          = _bgi_Dao_;

    FakeDao = class FakeDao extends Dao {
      constructor() {
        super();
      }

      getAPIEndpoint() {
        return '/api/fake';
      }
    };
  }));

	 afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
   });

  describe('.getAPIEndpoint()', function() {
    it('must be implemented in a subclass.', function() {
      let dao = new Dao();
      expect(function() {
        dao.getAPIEndpoint();
      }).toThrowError('getAPIEndpoint() not implemented.');
    });
  });

  describe('.get()', function() {
    it('gets a single record using $resource.get()', function(done) {
      let dao = new FakeDao();

      $httpBackend
        .expectGET('/api/fake?id=4')
        .respond({fakeID: 4});

      dao
        .get({id: 4})
        .then(res => {
          expect(res.fakeID).toBe(4);
          done();
        })
        .catch(() => expect(true).toBe(false));

      $httpBackend.flush();
    });
  });

  describe('.query()', function() {
    it('gets a single record using $resource.query().', function(done) {
      let dao = new FakeDao();

      $httpBackend
        .expectGET('/api/fake?q=stuff')
        .respond([{fakeID: 4}, {fakeID: 5}]);

      dao
        .query({q: 'stuff'})
        .then(res => {
          expect(res.length).toBe(2);
          done();
        })
        .catch(() => expect(true).toBe(false));

      $httpBackend.flush();
    });
  });

  describe('.save()', function() {
    it('saves a record using $resource.save()).', function(done) {
      let dao  = new FakeDao();
      let fake = {fakeID: 4, name: 'stuff'};

      $httpBackend
        .expectPOST('/api/fake', fake)
        .respond(200, {success: true});

      dao
        .save({fakeID: 4, name: 'stuff'})
        .then(res => {
          expect(res.success).toBe(true);
          done();
        })
        .catch(() => expect(true).toBe(false));

      $httpBackend.flush();
    });
  });

  describe('.replace()', function() {
    it('replaces records using PUT.', function(done) {
      let dao  = new FakeDao();
      let fake = [{fakeID: 4, name: 'stuff'}];

      $httpBackend
        .expectPUT('/api/fake?exhibitorID=42', fake)
        .respond(200, fake);

      dao
        .replace({exhibitorID: 42}, fake)
        .then(res => {
          expect(res[0].fakeID).toBe(fake[0].fakeID);
          done();
        })
        .catch(err => console.error(err.message));

      $httpBackend.flush();
    });
  });

  describe('.delete()', function() {
    it('deletes a single record using $resource.delete()', function(done) {
      let dao = new FakeDao();

      $httpBackend
        .expectDELETE('/api/fake?id=4')
        .respond({recordCount: 1});

      dao
        .delete({id: 4})
        .then(res => {
          expect(res.recordCount).toBe(1);
          done();
        })
        .catch(() => expect(true).toBe(false));

      $httpBackend.flush();
    });
  });
});

