(function(angular) {
  'use strict';

  angular.module('bgi')
    .factory('bgi_Loader', [loaderProducer])
    // Single system-wide loader.
    .factory('bgi_loader', ['bgi_Loader', Loader => new Loader()]);

  function loaderProducer() {
    /**
     * A utility to utility to aid with asynchronous tasks.
     */
    class Loader {
      /**
       * Initialize the loader.
       */
      constructor() {
        this.loading   = false;
        this._promises = [];
      }

      /**
       * Load until a promise is resolved/rejected.  If there are existing
       * promises this one is queued up.
       * @param prom The promise to wait on.
       */
      loadUntil(prom) {
        // If this promise has already been passed ignore it.
        if (this._promises.indexOf(prom) !== -1) {
          return;
        }

        this.loading = true;
        this._promises.push(prom);

        prom
          .finally(() => {
            this._promises.splice(this._promises.indexOf(prom), 1);

            if (this._promises.length === 0) {
              this.loading = false;
            }
          });
      }

      /**
       * Returns loading state
       * @returns {boolean} true=loading, false=not
       */
      isLoading() {
        return this.loading;
      }
    }

    return Loader;
  }
})(window.angular);

