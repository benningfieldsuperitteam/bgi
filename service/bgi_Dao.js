(function(angular) {
  'use strict';

  angular.module('bgi')
    .factory('bgi_Dao', ['$resource', DaoProducer]);

  function DaoProducer($resource) {
    /** Base-class for data-access objects. */
    class Dao {
      /**
       * Init.
       */
      constructor() {
      }

      /**
       * Get the API endpoint.
       */
      getAPIEndpoint() {
        throw new Error('getAPIEndpoint() not implemented.');
      }

      /**
       * Get a single record.
       */
      get(params) {
        return $resource(this.getAPIEndpoint())
          .get(params)
          .$promise;
      }

      /**
       * Get an array of records.
       */
      query(params) {
        return $resource(this.getAPIEndpoint())
          .query(params)
          .$promise;
      }

      /**
       * Save (update or create) a record.
       */
      save(record) {
        return $resource(this.getAPIEndpoint())
          .save(record)
          .$promise;
      }

      /**
       * Delete a single record.
       */
      delete(params) {
        return $resource(this.getAPIEndpoint())
          .delete(params)
          .$promise;
      }

      /**
       * Replace an array of child records (e.g. all phone numbers for a
       * person).
       */
      replace(parent, record) {
        return $resource(this.getAPIEndpoint(), null, {
            replace : {
              method  : 'PUT',
              isArray : true
            }
          })
          .replace(parent, record)
          .$promise;
      }
    }

    return Dao;
  }
})(window.angular);

