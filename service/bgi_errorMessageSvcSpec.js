describe('ErrorMessageSvc()', function() {
  'use strict';

  let errMsgSvc, $rootScope, $q;

  beforeEach(module('bgi'));
  beforeEach(inject(function(_bgi_errorMessageSvc_, _$rootScope_, _$q_) {
    errMsgSvc  = _bgi_errorMessageSvc_;
    $rootScope = _$rootScope_;
    $q         = _$q_;
  }));

  describe('.standardizeError()', function() {
    it('checks that when no error is passed in the promise is resolved.', function() {
      errMsgSvc
        .standardizeError()
        .then(() => expect(true).toBe(true))
        .catch(() => expect(true).toBe(false));
      $rootScope.$apply();
    });

    it('checks that if err is a rejected promise the returned promise is rejected.', function() {
      const err = new Error('Mock');

      errMsgSvc
        .standardizeError($q.reject(err))
        .then(() => expect(true).toBe(false))
        .catch(e => expect(e.detail).toBe('Mock'));
      $rootScope.$apply();
    });

    it('checks that if err is a resolved promise the returned promise is resolved.', function() {
      errMsgSvc
        .standardizeError($q.resolve())
        .then(() => expect(true).toBe(true))
        .catch(() => expect(true).toBe(false));
      $rootScope.$apply();
    });

    it('checks that standard error instances get converted.', function() {
      const err = new Error('Mock');

      errMsgSvc
        .standardizeError(err)
        .then(() => expect(true).toBe(false))
        .catch(e => expect(e.detail).toBe('Mock'));
      $rootScope.$apply();
    });

    it('checks that the error code is preserved.', function() {
      const err = {code: 'CODE', detail: 'DET'};

      errMsgSvc
        .standardizeError(err)
        .then(() => expect(true).toBe(false))
        .catch(e => {
          expect(e.detail).toBe('DET');
          expect(e.code).toBe('CODE');
        });
      $rootScope.$apply();
    });

    it('checks that detail takes precedence over message.', function() {
      const err = {detail: 'DET', message: 'MSG'};

      errMsgSvc
        .standardizeError(err)
        .then(() => expect(true).toBe(false))
        .catch(e => {
          expect(e.detail).toBe('DET');
          expect(e.message).not.toBeDefined();
        });
      $rootScope.$apply();
    });

    it('checks that sub errors are converted.', function() {
      const err = {
        detail  : 'DET',
        code    : 'CODE',
        errors  : [
          {
            detail : 'DET0',
            code   : 'CODE0',
            errors : []
          },
          {
            detail : 'DET1',
            code   : 'CODE0',
            errors : []
          }
        ]
      };

      errMsgSvc
        .standardizeError(err)
        .then(() => expect(true).toBe(false))
        .catch(e => expect(e).toEqual(err));
      $rootScope.$apply();
    });
  });
});
